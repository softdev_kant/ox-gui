package oxoop;

public class Player {

    private char symbol;
    private int win;
    private int loss;
    private int draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public void win() {
        this.win++;
    }

    public void loss() {
        this.loss++;
    }

    @Override
    public String toString() {
        return "Player " + symbol + " win: " + win + " loss: " + loss + " draw: " + draw;
    }

    public void draw() {
        this.draw++;
    }

    String getWin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    String getDraw() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    String getLoss() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
